import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        double average;
        Scanner scanner = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = scanner.nextLine();

        System.out.println("Last Name:");
        lastName = scanner.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = scanner.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = scanner.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = scanner.nextDouble();

        average = (firstSubject+secondSubject+thirdSubject)/3;
        System.out.printf("Good day, %s %s %nYour grade average is: %d", firstName, lastName, (int) average);

    }
}